# Словьанѣца / Марѹсьа / Кирилица

![Възходнословьанско рӑзложеньѥ клавіатѹръі дльа кирилици - Марѹсьа](https://gitlab.com/Translator5/marousja)
<img src="/release/Linux/marousja.png"
      alt="closeup"
      width="800"/>

Възходнословьанско рӑзложеньѥ клавіатѹръі дльа глаголици.
<img src="/release/Linux/glagolica.png"
      alt="closeup"
      width="800"/>

Европейско рӑзложеньѥ клавіатѹръі дльа латиници, базоваше сѧ на 'E1'.
<img src="/release/Linux/latinica.png"
      alt="closeup"
      width="800"/>

Европейско рӑзложеньѥ клавіатѹръі дльа кирилици, базоваше сѧ на 'E1'.
<img src="/release/Linux/kirilica.png"
      alt="closeup"
      width="800"/>

Европейско рӑзложеньѥ клавіатѹръі дльа глаголици, базоваше сѧ на 'E1'.
<img src="/release/Linux/kyrilovica.png"
      alt="closeup"
      width="800"/>


<img src="/release/Android/snimoko_1.jpg"
      alt="closeup"
      width="400"/>
<img src="/release/Android/snimoko_2.jpg"
      alt="closeup"
      width="400"/>

Рӑзложеньѥ клавіатѹръі дльа Андройдъ

Рӑзложеньѥ клавіатѹръі дльа Виндовсъ
<img src="/release/Windows/win1.png"
      alt="closeup"
      width="800"/>
<img src="/release/Windows/win2.png"
      alt="closeup"
      width="800"/>

![Рӑзширьено рӑзложеньѥ клавіатѹръі - Марѹсьа дльа Андройдъ (Simple Keyboard - Apache License 2.0)](https://gitlab.com/Translator5/simpleslavickeyboard)

Дльа `.klc`–докѹмента бъілъ корістанъ ![Keyboard Layout Converter(GNU GPL v3)](https://github.com/Translator5/keyboard-layout-converter)
![Словникъ](https://interslavic-dictionary.com)


## Ѹставьенѣѥ подъ Линѹксъ
1. Внеси док. `.xkb` до `/usr/share/X11/xkb/symbols`
(sudo cp sla /usr/share/X11/xkb/symbols)

2. Добави въ `.xml`–файлъі: `/usr/share/X11/xkb/rules/base.xml` и `/usr/share/X11/xkb/rules/evdev.xml` сеѭ припись;

```
    <layout>
      <configItem>
        <name>sla</name>
<!-- Keyboard indicator for Slavic layouts -->
        <shortDescription>мс</shortDescription>
        <description>Словјанскъі</description>
        <languageList>
          <iso639Id>sla</iso639Id>
        </languageList>
      </configItem>
      <variantList>
        <variant>
          <configItem>
            <name>sla_MS</name>
            <description>(Марѹсьа)</description>
          </configItem>
        </variant>
        <variant>
          <configItem>
            <name>sla_GS</name>
            <description>(Ⰳⰾⰰⰳⱁⰾⰻⱌⰰ)</description>
          </configItem>
        </variant>
        <variant>
          <configItem>
            <name>sla_ISV</name>
            <description>(Latinica E1)</description>
          </configItem>
        </variant>
        <variant>
          <configItem>
            <name>sla_ZLS</name>
            <description>(Кирилица Е1)</description>
          </configItem>
        </variant>
        <variant>
          <configItem>
            <name>sla_GSV</name>
            <description>(Ⰽⱛⱃⰺⰾⰾⱁⰲⰺⱌⰰ Ⰵⰰ҃)</description>
          </configItem>
        </variant>
      </variantList>
    </layout>
```


## Кодъі дльа ѩзъіковъ
ISO 639-2/639-5: sla			// Все словьанскъі ѩзъіки

ISO 639-2/639-5: zle, zls, zlw	// Възходни, южни и западни словьански

ISO 639-3/639-2/639-5: chu		// Цркъвнословьанскъі

ISO 639-1: cu					// Цркъвнословьанскъі

ISO 639 ...: isv				// Мећѹслоꙗнскъі, Interslavic

Glottolog: inte1263				// Мећѹслоꙗнскъі, Interslavic

ISO 15924: Cyrl, Cyrs, Glag 	// Кирилица,

### Мои нововведеньѥ (кодъі дльа разложеньꙗ и граматики)
✓ sla_MS: Словьанскъі (Марѹсьа)

✓ sla_GS: Ⱄⰾⱁⰲⱜⰰⱀⱄⰽⱏⰹ (Ⰳⰾⰰⰳⱁⰾⰻⱌⰰ)

✓ sla_ISV: Medžuslovjansky (Latinica E1)

✓ sla_ZLS: Мећусловјанскъі (Кирилица Е1)

✓ sla_GSV: Ⱄⰾⱁⰲⱜⰰⱀⱄⰽⱏⰹ (Ⰽⱛⱃⰺⰾⰾⱁⰲⰺⱌⰰ Ⰵⰰ҃)

sla_CHU: Црькъвьнословѣньскъ


![card-alpha](/stuff/ToKako.png)

2018-06-12 Корцаковъ, Романъ
